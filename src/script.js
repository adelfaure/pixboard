// REZ

let width = 256;
let height = 256;

// CANVAS
const canvas = document.getElementById('canvas');
canvas.width = width;
canvas.height = height;

// CONTEXT
const ctx = canvas.getContext('2d');

// IMAGEDATA
var Cursor = ctx.createImageData(width,height);
var UI = ctx.createImageData(width,height);
var Scene = ctx.createImageData(width,height);
var Display = ctx.createImageData(width,height);


// CLEAR CANVAS
function clearCanvas(){
  ctx.clearRect(0,0,width,height);
}

// CLEAR IMAGEDATA
function clearImageData(imageData){
  for (var i = 0; i < imageData.data.length; i++) {
    imageData.data[i] = 0;
  }
}

// ADD IMAGEDATATO
function addImageDataTo(imageDataA,imageDataB) {
  for (var i = 0; i < imageDataA.data.length; i+=4) {
    if (imageDataA.data[i+3] != 0) {
      imageDataB.data[i] = imageDataA.data[i];
      imageDataB.data[i+1] = imageDataA.data[i+1];
      imageDataB.data[i+2] = imageDataA.data[i+2];
      imageDataB.data[i+3] = imageDataA.data[i+3];
    }
  }
}

// CURSOR
let cursor_x = 0;
let cursor_y = 0;
let cursor_w = 8;
let cursor_h = 8;

// COLOR

const azerty_active_keys = ['&','é','"','\'','(','-','è','_','ç','à',')','=','a','z','e','r','t','y','u','i','o','p','Dead','$','q','s','d','f','g','h','j','k','l','m','ù','*','<','w','x','c','v','b','n',',',';',':','!','1','2','3','4','5','6','7','8','9','0','°','+','A','Z','E','R','T','Y','U','I','O','P','¨','£','Q','S','D','F','G','H','J','K','L','M','%','µ','>','W','X','C','V','B','N','?','.','/','§','~','#','{','[','|','`','\\','^','@',']','}','¤'];

const qwerty_active_keys = ["'","1","2","3","4","5","6","7","8","9","0","-","=","q","w","e","r","t","y","u","i","o","p","[","]","\\","a","s","d","f","g","h","j","k","l",";","'","z","x","c","v","b","n","m",",",".","/","~","!","@","#","$","%","^","&","*","(",")","_","+","Q","W","E","R","T","Y","U","I","O","P","{","}","l","A","S","D","F","G","H","J","K","L",":","\"","Z","X","C","V","B","N","M","<",">","?"];
/*
const active_colors = [];
const active_colors_str = [];

let turn = [
  [false,false,false],
  [false,false,true],
  [false,true,false],
  [false,true,true],
  [true,false,false],
  [true,false,true],
  [true,true,false],
  [true,true,true],
];

let gap = 85;

for (let a = 0; a < turn.length; a++) {
  let a_value = [0,0,0];
  a_value[0] = turn[a][0] ? gap : 0;
  a_value[1] = turn[a][1] ? gap : 0;
  a_value[2] = turn[a][2] ? gap : 0;
  for (let b = 0; b < turn.length; b++) {
    let b_value = [0,0,0];
    b_value[0] = turn[b][0] ? a_value[0] + gap : a_value[0];
    b_value[1] = turn[b][1] ? a_value[1] + gap : a_value[1];
    b_value[2] = turn[b][2] ? a_value[2] + gap : a_value[2];
    for (let c = 0; c < turn.length; c++) {
      let c_value = [0,0,0];
      c_value[0] = turn[c][0] ? b_value[0] + gap : b_value[0];
      c_value[1] = turn[c][1] ? b_value[1] + gap : b_value[1];
      c_value[2] = turn[c][2] ? b_value[2] + gap : b_value[2];
      if (!active_colors_str.includes(''+c_value[0]+c_value[1]+c_value[2])) {
        active_colors_str.push(''+c_value[0]+c_value[1]+c_value[2]);
        active_colors.push(c_value);
      }
    }
  }
}

active_colors.unshift(active_colors.pop());

const colors = {
}

for (let i = 0; i < active_colors.length; i++) {
  colors[active_keys[i]] = [0,0,0,0];
  colors[active_keys[i]][0] = active_colors[i][0];
  colors[active_keys[i]][1] = active_colors[i][1];
  colors[active_keys[i]][2] = active_colors[i][2];
  colors[active_keys[i]][3] = 255;
}

colors[' '] = [0,0,0,0];

let color_pick = 'a';
*/
// APPLY COLOR
function apply_color_to_img(img,x,y,r,v,b,a) {
  img.data[(x+y*img.width)*4] = r;
  img.data[(x+y*img.width)*4+1] = v;
  img.data[(x+y*img.width)*4+2] = b;
  img.data[(x+y*img.width)*4+3] = a;
}

// COLORIZE
function colorize(img,x,y,r,v,b,a) {
  let pixel = get_color_from_img(img,x,y);
  if (pixel[0] != to_colorize[0]
  ||  pixel[1] != to_colorize[1]
  ||  pixel[2] != to_colorize[2]
  ||  pixel[3] != to_colorize[3]) return false;
  img.data[(x+y*img.width)*4] = r;
  img.data[(x+y*img.width)*4+1] = v;
  img.data[(x+y*img.width)*4+2] = b;
  img.data[(x+y*img.width)*4+3] = a;
}

// GET COLOR
function get_color_from_img(img,x,y) {
  return [
    img.data[(x+y*img.width)*4],
    img.data[(x+y*img.width)*4+1],
    img.data[(x+y*img.width)*4+2],
    img.data[(x+y*img.width)*4+3] 
  ];
}

// KEYS
var keys = {};
let key_down = false;
let key_pressed = false;
let mode = 0;
let modes = ["draw","paste","color"];

// KEYBOARD
document.addEventListener("keydown",function(e){
  e.preventDefault();
  e.stopPropagation();
  key_pressed = e.key;
  console.log(e.key);
  if (keys["Shift"] && key_pressed == "Dead") {
    keys['¨'] = true;
  } else {
    keys[e.key] = true;
  }
  key_down = true;
  if (e.key == "Tab") {
    mode = mode < modes.length-1 ? mode + 1 : 0;
  }
});

document.addEventListener("keyup",function(e){
  keys[e.key] = false;
  key_down = false;
});

// LOAD
let input = document.createElement("input");
input.type = "file";
input.accept = ".png";
input.addEventListener("change",function(e){
  let file = input.files[0];
  let fr = new FileReader();
  fr.addEventListener("load", function(){
    console.log(fr);
    img = new Image();
    img.src = fr.result;
    img.addEventListener("load",function(){
      clearCanvas();
      ctx.drawImage(this, 0, 0);
      let new_ImageData = ctx.getImageData(0,0,width,height);
      for (var i = 0; i < new_ImageData.data.length; i++) {
        Scene.data[i] = new_ImageData.data[i];
      }
      new_memory(Scene);
    },false);
  }, false);
  fr.readAsDataURL(file); 
});

// SAVE  
function DownloadCanvasAsImage(){
  let downloadLink = document.createElement('a');
  downloadLink.setAttribute('download', 'pixboard.png');
  ctx.putImageData(Scene, 0, 0);
  let canvas = document.getElementById('canvas');
  canvas.toBlob(function(blob) {
    let url = URL.createObjectURL(blob);
    downloadLink.setAttribute('href', url);
    downloadLink.click();
  });
}

// CLIPBOARD

let fly_clipboard = {
  'v':[]
}

function copy_to(key,pos_x,pos_y,width,height,data,clipboard) {
  if (!clipboard[key]) return false;
  clipboard[key] = [];
  for (var y = 0; y < height; y++) {
    line = [];
    for (var x = 0; x < width; x++) {
      line.push(get_color_from_img(data,pos_x + x,pos_y + y));
    }
    clipboard[key].push(line);
  }
  console.log("copied to ", key);
}

var color = [0,0,0];
var target_color = false;

function paste_from(key, clipboard, start_x, start_y, local_width, local_height, local_color, local_target_color, ImageData, keepcolor) {
  if (!clipboard[key] || !clipboard[key].length) return false;
  let copied = clipboard[key];
  let y_offset = 0;
  for (var y = 0; y < local_height; y++) {
    if (y + start_y >= height ) {
      continue;
    }
    if (y + y_offset > copied.length-1) {
      y_offset -= copied.length;
    }
    let x_offset = 0;
    for (var x = 0; x < local_width; x++) {
      if (x + start_x >= width ) {
        continue;
      }
      if (x + x_offset > copied[y + y_offset].length-1) {
        x_offset -= copied[y + y_offset].length;
      }
      if (local_target_color) {
        let pixel = get_color_from_img(Scene,start_x + x, start_y + y);
        if (pixel[0] != local_target_color[0]
        ||  pixel[1] != local_target_color[1]
        ||  pixel[2] != local_target_color[2]
        ||  pixel[3] != local_target_color[3]) continue;
      }
      if (copied[y + y_offset][x + x_offset][3] == 0 ) {
        continue;
      }
      if (keepcolor) {
        apply_color_to_img(ImageData, start_x + x, start_y + y, copied[y + y_offset][x + x_offset][0], copied[y + y_offset][x + x_offset][1], copied[y + y_offset][x + x_offset][2],255);  
      } else {
        apply_color_to_img(ImageData, start_x + x, start_y + y, local_color[0], local_color[1], local_color[2],255);  
      }
    }
  }
}

// INIT ASSETS

// intro screen
intro_screen_img = new Image();
intro_screen_img.src = intro_screen_64;

// keybindings
keybindings_img = new Image();
keybindings_img.src = keybindings_64;

// azerty graphic mode
let azerty_graphic_mode_img = new Image();
azerty_graphic_mode_img.src = graphic_mode_azerty_64;

// qwerty graphic mode
let qwerty_graphic_mode_img = new Image();
qwerty_graphic_mode_img.src = graphic_mode_qwerty_64;

// azerty modes
let azerty_modes_img = new Image();
azerty_modes_img.src = modes_azerty_64;

// qwerty modes
let qwerty_modes_img = new Image();
qwerty_modes_img.src = modes_qwerty_64;

// azerty colors
let azerty_color_mode_img = new Image();
azerty_color_mode_img.src = color_mode_azerty_64;

// qwerty colors
let qwerty_color_mode_img = new Image();
qwerty_color_mode_img.src = color_mode_qwerty_64;

// azerty text mode
let azerty_text_mode_img = new Image();
azerty_text_mode_img.src = text_mode_azerty_64;

// qwerty text mode
let qwerty_text_mode_img = new Image();
qwerty_text_mode_img.src = text_mode_qwerty_64;


// LOAD ASSETS

// azerty graphic mode
const azerty_graphic_mode_set = {}
for (let i = 0; i < azerty_active_keys.length; i++) {
  azerty_graphic_mode_set[azerty_active_keys[i]] = [];
}
var azerty_graphic_mode_loaded = false;
azerty_graphic_mode_img.addEventListener("load",function(){
  clearCanvas();
  ctx.drawImage(azerty_graphic_mode_img,0,0,width,height);
  var buffer = ctx.getImageData(0,0,width,height);
  var buffer_reader_x = 0;
  var buffer_reader_y = 0;
  for (var i = 0; i < azerty_active_keys.length; i++) {
    buffer_reader_x = buffer_reader_x / 8 < 32 ? buffer_reader_x + 8 : 8;
    buffer_reader_y = buffer_reader_x == 8 ? buffer_reader_y + 8 : buffer_reader_y;
    copy_to(azerty_active_keys[i], buffer_reader_x - 8, buffer_reader_y-8, 8, 8, buffer, azerty_graphic_mode_set);
  }
  azerty_graphic_mode_loaded = true;
},false);

// qwerty graphic mode
const qwerty_graphic_mode_set = {}
for (let i = 0; i < qwerty_active_keys.length; i++) {
  qwerty_graphic_mode_set[qwerty_active_keys[i]] = [];
}
var qwerty_graphic_mode_loaded = false;
qwerty_graphic_mode_img.addEventListener("load",function(){
  clearCanvas();
  ctx.drawImage(qwerty_graphic_mode_img,0,0,width,height);
  var buffer = ctx.getImageData(0,0,width,height);
  var buffer_reader_x = 0;
  var buffer_reader_y = 0;
  for (var i = 0; i < qwerty_active_keys.length; i++) {
    buffer_reader_x = buffer_reader_x / 8 < 32 ? buffer_reader_x + 8 : 8;
    buffer_reader_y = buffer_reader_x == 8 ? buffer_reader_y + 8 : buffer_reader_y;
    copy_to(qwerty_active_keys[i], buffer_reader_x - 8, buffer_reader_y-8, 8, 8, buffer, qwerty_graphic_mode_set);
  }
  qwerty_graphic_mode_loaded = true;
},false);

// azerty color mode
const azerty_color_mode_set = {}
for (let i = 0; i < azerty_active_keys.length; i++) {
  azerty_color_mode_set[azerty_active_keys[i]] = [];
}
var azerty_color_mode_loaded = false;
azerty_color_mode_img.addEventListener("load",function(){
  clearCanvas();
  ctx.drawImage(azerty_color_mode_img,0,0,width,height);
  var buffer = ctx.getImageData(0,0,width,height);
  var buffer_reader_x = 0;
  var buffer_reader_y = 0;
  for (var i = 0; i < azerty_active_keys.length; i++) {
    buffer_reader_x = buffer_reader_x / 8 < 32 ? buffer_reader_x + 8 : 8;
    buffer_reader_y = buffer_reader_x == 8 ? buffer_reader_y + 8 : buffer_reader_y;
    copy_to(azerty_active_keys[i], buffer_reader_x - 8, buffer_reader_y-8, 8, 8, buffer, azerty_color_mode_set);
  }
  azerty_color_mode_loaded = true;
},false);

// qwerty color mode
const qwerty_color_mode_set = {}
for (let i = 0; i < qwerty_active_keys.length; i++) {
  qwerty_color_mode_set[qwerty_active_keys[i]] = [];
}
var qwerty_color_mode_loaded = false;
qwerty_color_mode_img.addEventListener("load",function(){
  clearCanvas();
  ctx.drawImage(qwerty_color_mode_img,0,0,width,height);
  var buffer = ctx.getImageData(0,0,width,height);
  var buffer_reader_x = 0;
  var buffer_reader_y = 0;
  for (var i = 0; i < qwerty_active_keys.length; i++) {
    buffer_reader_x = buffer_reader_x / 8 < 32 ? buffer_reader_x + 8 : 8;
    buffer_reader_y = buffer_reader_x == 8 ? buffer_reader_y + 8 : buffer_reader_y;
    copy_to(qwerty_active_keys[i], buffer_reader_x - 8, buffer_reader_y-8, 8, 8, buffer, qwerty_color_mode_set);
  }
  qwerty_color_mode_loaded = true;
},false);

// azerty text mode
const azerty_text_mode_set = {}
for (let i = 0; i < azerty_active_keys.length; i++) {
  azerty_text_mode_set[azerty_active_keys[i]] = [];
}
var azerty_text_mode_loaded = false;
azerty_text_mode_img.addEventListener("load",function(){
  clearCanvas();
  ctx.drawImage(azerty_text_mode_img,0,0,width,height);
  var buffer = ctx.getImageData(0,0,width,height);
  var buffer_reader_x = 0;
  var buffer_reader_y = 0;
  for (var i = 0; i < azerty_active_keys.length; i++) {
    buffer_reader_x = buffer_reader_x / 8 < 32 ? buffer_reader_x + 8 : 8;
    buffer_reader_y = buffer_reader_x == 8 ? buffer_reader_y + 8 : buffer_reader_y;
    copy_to(azerty_active_keys[i], buffer_reader_x - 8, buffer_reader_y-8, 8, 8, buffer, azerty_text_mode_set);
  }
  azerty_text_mode_loaded = true;
},false);

// qwerty text mode
const qwerty_text_mode_set = {}
for (let i = 0; i < qwerty_active_keys.length; i++) {
  qwerty_text_mode_set[qwerty_active_keys[i]] = [];
}
var qwerty_text_mode_loaded = false;
qwerty_text_mode_img.addEventListener("load",function(){
  clearCanvas();
  ctx.drawImage(qwerty_text_mode_img,0,0,width,height);
  var buffer = ctx.getImageData(0,0,width,height);
  var buffer_reader_x = 0;
  var buffer_reader_y = 0;
  for (var i = 0; i < qwerty_active_keys.length; i++) {
    buffer_reader_x = buffer_reader_x / 8 < 32 ? buffer_reader_x + 8 : 8;
    buffer_reader_y = buffer_reader_x == 8 ? buffer_reader_y + 8 : buffer_reader_y;
    copy_to(qwerty_active_keys[i], buffer_reader_x - 8, buffer_reader_y-8, 8, 8, buffer, qwerty_text_mode_set);
  }
  qwerty_text_mode_loaded = true;
},false);

// CURSOR

function cursor_move() {
  cursor_x = !keys["Control"] && keys["ArrowRight"] ? cursor_x + cursor_w :
             !keys["Control"] && keys["ArrowLeft"]  ? cursor_x - cursor_w :
             cursor_x;
  cursor_y = !keys["Control"] && keys["ArrowDown"] ? cursor_y +  cursor_h :
             !keys["Control"] && keys["ArrowUp"]  ? cursor_y - cursor_h :
             cursor_y;
  cursor_h = keys["Control"] && keys['ArrowUp'] ? cursor_h * 2 :
             keys["Control"] && keys['ArrowDown'] ? cursor_h / 2 : 
             cursor_h;
  cursor_w = keys["Control"] && keys['ArrowRight'] ? cursor_w * 2 : 
             keys["Control"] && keys['ArrowLeft'] ? cursor_w / 2 : 
             cursor_w;
  cursor_w = cursor_w < 1 ? 1 : cursor_w;
  cursor_w = cursor_w > width ? width : cursor_w;
  cursor_h = cursor_h < 1 ? 1 : cursor_h;
  cursor_h = cursor_h > width ? width : cursor_h;
  cursor_x = cursor_x >= width ? cursor_x - cursor_w : cursor_x;
  cursor_x = cursor_x < 0 ? cursor_x + cursor_w : cursor_x;
  cursor_y = cursor_y >= height ? cursor_y - cursor_h : cursor_y;
  cursor_y = cursor_y < 0 ? cursor_y + cursor_h : cursor_y;
  cursor_x = Math.floor(cursor_x / cursor_w) * cursor_w;
  cursor_y = Math.floor(cursor_y / cursor_h) * cursor_h;
  keys["ArrowRight"] = false;
  keys["ArrowLeft"] = false;
  keys["ArrowDown"] = false;
  keys["ArrowUp"] = false;
}

function cursor_blink() {  
  clearImageData(Cursor);
  for (var y = 0; y < cursor_h; y++) {
    line = [];
    for (var x = 0; x < cursor_w; x++) {
      line.push(get_color_from_img(Scene,cursor_x + x,cursor_y + y));
      if (Number.isInteger(Math.floor(frame/50)/2)) {
        apply_color_to_img(Cursor,cursor_x + x,cursor_y + y,255,255,255,255);  
      } else {
        apply_color_to_img(Cursor,cursor_x + x,cursor_y + y,0,0,0,255);  
      }
    }
  }
}

// PRINT

function print() {
  clearImageData(Display);
  addImageDataTo(Scene,Display);
  addImageDataTo(Cursor,Display);
  addImageDataTo(UI,Display);
  ctx.putImageData(Display, 0, 0);
}

// WRITE

function write(str, start_x, start_y){
  for (var i = 0; i < str.length; i++) {
    paste_from(str[i],azerty_text_mode_set, start_x + i*8, start_y, 8, 8, [0,0,0], false, UI);
  }
}

// MEMORY
let back_memory = [];
let front_memory = [];

function new_memory(ImageData) {
  back_memory.push(new Uint8ClampedArray(ImageData.data));
  while (front_memory.length) front_memory.pop();
}

// Floodfill

let dirs = [[1,0], [0,1], [-1,0], [0,-1]];

function floodfill(ImageData, x, y, color, target_color, recursion) {
  if (!recursion) {
    recursion = 0;
  } else {
    recursion++;
  }
  let local_color = get_color_from_img(ImageData, x, y);
  if (!target_color) {
    target_color = local_color;
  } else {
    if (local_color[0] != target_color[0]
     || local_color[1] != target_color[1]
     || local_color[2] != target_color[2]
     || local_color[3] != target_color[3]
    ) {
      return;
    }
  }
  apply_color_to_img(ImageData, x, y, color[0], color[1], color[2], color[3]);
  for (var i = 0; i < dirs.length; i++) {
    let new_x = x+dirs[i][0];
    let new_y = y+dirs[i][1];
    if (new_x < 0
     || new_y < 0
     || new_x >= width
     || new_y >= height
    ) {
      continue;
    }
    let next_color = get_color_from_img(ImageData,new_x,new_y);
    if (next_color[0] == color[0]
     && next_color[1] == color[1]
     && next_color[2] == color[2]
     && next_color[3] == color[3]
    ) {
      continue;
    }
    if (recursion < 1000) {
      floodfill(ImageData, new_x, new_y, color, target_color, recursion+1);
    } else {
      setTimeout(function(){
        floodfill(ImageData, new_x, new_y, color, target_color)
      });
    }
  }
}
         
// DRAW  
var copied = [];
let frame = 0;
let log_clear_time = 100;
let to_colorize = [0,0,0,0];
let set = qwerty_color_mode_set;
let tab = false;
let layout = true;
//paste_from('&',azerty_graphic_mode_set, 0, 0, width, 8, [127,127,127], false, UI);
//write(tab ? "Graphic mode" : "Text mode",0,0);
function draw() {
  if (frame == log_clear_time) {
    clearImageData(UI);
  }
  if (keys["Tab"]) {
    tab = !tab;
    keys["Tab"] = false;
    paste_from('&',azerty_graphic_mode_set, 0, 0, width, 8, [127,127,127], false, UI);
    write(tab ? "Graphic mode" : "Text mode",0,0);
    log_clear_time = frame + 100;
  }
  if (keys["F1"]) {
    layout = !layout;
    keys["F1"] = false;
    paste_from('&',azerty_graphic_mode_set, 0, 0, width, 8, [127,127,127], false, UI);
    write(layout ? "Keyboard layout QWERTY" : "Keyboard layout AZERTY",0,0);
    log_clear_time = frame + 100;
  }
  if (keys["Delete"]) {
    clearImageData(Scene);
    new_memory(Scene);
    keys["Delete"] = false;
    paste_from('&',azerty_graphic_mode_set, 0, 0, width, 8, [127,127,127], false, UI);
    write("Scene clear",0,0);
    log_clear_time = frame + 100;
    setTimeout(draw,17);
    return;
  }
  if (keys["PageUp"]) {
    DownloadCanvasAsImage();
    keys["PageUp"] = false;
    setTimeout(draw,17);
    return;
  }
  if (keys["PageDown"]) {
    paste_from('&', azerty_graphic_mode_set, 0, 0, width, 8, [127,127,127], false, UI);
    write("If import fail press 'Enter'",0,0);
    paste_from('&', azerty_graphic_mode_set, 0, 8, width, 8, [127,127,127], false, UI);
    write("then 'PageDown'",0,8);
    log_clear_time = frame + 100;
    input.click();
    keys["PageDown"] = false;
    setTimeout(draw,17);
    return;
  }
  if (keys["Insert"]) {
    floodfill(Scene,cursor_x,cursor_y,[color[0],color[1],color[2],255]);
    keys["Insert"] = false;
    setTimeout(function(){
      new_memory(Scene);
      console.log("NEW MEMORY");
    },100);
  }
  if (keys["Backspace"]) {
    cursor_x -= cursor_w;
    for (var y = 0; y < cursor_h; y++) {
      for (var x = 0; x < cursor_w; x++) {
        if (target_color) {
          let pixel = get_color_from_img(Scene, cursor_x + x, cursor_y + y);
          if (pixel[0] != target_color[0]
          ||  pixel[1] != target_color[1]
          ||  pixel[2] != target_color[2]
          ||  pixel[3] != target_color[3]) continue;
        }
        apply_color_to_img(Scene,cursor_x + x,cursor_y + y,0,0,0,0);  
      }
    }
    keys["Backspace"] = false;
    new_memory(Scene);
  }
  if (keys[" "]) {
    for (var y = 0; y < cursor_h; y++) {
      for (var x = 0; x < cursor_w; x++) {
        if (target_color) {
          let pixel = get_color_from_img(Scene, cursor_x + x, cursor_y + y);
          if (pixel[0] != target_color[0]
          ||  pixel[1] != target_color[1]
          ||  pixel[2] != target_color[2]
          ||  pixel[3] != target_color[3]) continue;
        }
        apply_color_to_img(Scene,cursor_x + x,cursor_y + y,0,0,0,0);  
      }
    }
    cursor_x += cursor_w;
    keys[" "] = false;
    new_memory(Scene);
  }
  if (keys["Control"]) {
    if (keys['z']){
      if (back_memory.length > 1) {
        front_memory.push(back_memory.pop());
      }
      let memory = back_memory[back_memory.length-1];
      for (var i = 0; i < memory.length; i++) {
        Scene.data[i] = memory[i];
      }
      keys['z'] = false;
    }
    if (keys['y']){
      let memory = front_memory[front_memory.length-1];
      if (front_memory.length > 0) {
        back_memory.push(front_memory.pop());
        for (var i = 0; i < memory.length; i++) {
          Scene.data[i] = memory[i];
        }
      } 
      keys['y'] = false;
    }
    if (keys['c']) {
      copy_to('v',cursor_x,cursor_y,cursor_w,cursor_h,Scene,fly_clipboard);
    }
    if (keys['v']) {
      paste_from('v',fly_clipboard, cursor_x, cursor_y, fly_clipboard['v'][0].length, fly_clipboard['v'].length, color, target_color, Scene, true);
      cursor_x += cursor_w;
      new_memory(Scene);
      keys['v'] = false;
    }
  } else if (keys["Alt"]) {
    if (layout) {
      for (var i = 0; i < qwerty_active_keys.length; i++) {
        if (keys[qwerty_active_keys[i]]) {
          if (!qwerty_color_mode_set[qwerty_active_keys[i]] || !qwerty_color_mode_set[qwerty_active_keys[i]].length) break; 
          color[0] = qwerty_color_mode_set[qwerty_active_keys[i]][0][0][0];
          color[1] = qwerty_color_mode_set[qwerty_active_keys[i]][0][0][1];
          color[2] = qwerty_color_mode_set[qwerty_active_keys[i]][0][0][2];
          paste_from('&', azerty_graphic_mode_set, 0, 0, width, 8, [127,127,127], false, UI);
          paste_from('&', azerty_graphic_mode_set, 72, 0, 8, 8, color, false, UI);
          write("Color is ",0,0);
          log_clear_time = frame + 100;
          console.log(color);
          break;
        }
      }
    } else {
      for (var i = 0; i < azerty_active_keys.length; i++) {
        if (keys[azerty_active_keys[i]]) {
          if (!azerty_color_mode_set[azerty_active_keys[i]] || !azerty_color_mode_set[azerty_active_keys[i]].length) break; 
          color[0] = azerty_color_mode_set[azerty_active_keys[i]][0][0][0];
          color[1] = azerty_color_mode_set[azerty_active_keys[i]][0][0][1];
          color[2] = azerty_color_mode_set[azerty_active_keys[i]][0][0][2];
          paste_from('&', azerty_graphic_mode_set, 0, 0, width, 8, [127,127,127], false, UI);
          paste_from('&', azerty_graphic_mode_set, 72, 0, 8, 8, color, false, UI);
          write("Color is ",0,0);
          log_clear_time = frame + 100;
          console.log(color);
          break;
        }
      }
    }
  } else if (keys["Home"]) {
    target_color = get_color_from_img(Scene,cursor_x,cursor_y);
      paste_from('&', azerty_graphic_mode_set, 0, 0, width, 8, [127,127,127], false, UI);
    if (target_color[3] == 0) {
      write("Target color is transparent",0,0);
    } else {
      paste_from('&', azerty_graphic_mode_set, 72+8*7, 0, 8, 8, target_color, false, UI);
      write("Target color is ",0,0);
    }
    log_clear_time = frame + 100;
  } else if (keys["End"]) {
    target_color = false;
    paste_from('&', azerty_graphic_mode_set, 0, 0, width, 9, [127,127,127], false, UI);
    write("No target color",0,0);
    log_clear_time = frame + 100;
  } else {
    if (layout) {
      for (var i = 0; i < qwerty_active_keys.length; i++) {
        if (keys[qwerty_active_keys[i]]) {
          paste_from(qwerty_active_keys[i],tab?qwerty_graphic_mode_set:qwerty_text_mode_set, cursor_x, cursor_y, cursor_w, cursor_h, color, target_color, Scene);
          cursor_x += cursor_w;
          keys[qwerty_active_keys[i]] = false;
          new_memory(Scene);
          break;
        }
      }
    } else {
      for (var i = 0; i < azerty_active_keys.length; i++) {
        if (keys[azerty_active_keys[i]]) {
          paste_from(azerty_active_keys[i],tab?azerty_graphic_mode_set:azerty_text_mode_set, cursor_x, cursor_y, cursor_w, cursor_h, color, target_color, Scene);
          cursor_x += cursor_w;
          keys[azerty_active_keys[i]] = false;
          new_memory(Scene);
          break;
        }
      }
    }
  }
  cursor_move();
  cursor_blink();
  if (keys["F4"]) {
    ctx.drawImage(layout?qwerty_modes_img : azerty_modes_img,0,0,width,height);
  } else if (keys["F2"]){
    ctx.drawImage(intro_screen_img,0,0,width,height);
  } else if (keys["F3"]){
    ctx.drawImage(keybindings_img,0,0,width,height);
  } else {
    print();
  }
  frame++;
  setTimeout(draw,17);
}

// SETUP
function loading_listener() {
  if (azerty_graphic_mode_loaded && azerty_color_mode_loaded && azerty_text_mode_loaded && qwerty_graphic_mode_loaded && qwerty_color_mode_loaded && qwerty_text_mode_loaded) {
    // generate background
    for (var i = 0; i < 1024; i ++) {
      let key = azerty_active_keys[Math.floor(Math.random()*azerty_active_keys.length)];
      let color_key = Object.keys(azerty_color_mode_set)[Math.floor(Math.random()*Object.keys(azerty_color_mode_set).length)];
      let rdm = Math.random() > 0.5 ? 1 : Math.random()*8;
      while (azerty_color_mode_set[color_key][0][0][3] == 0) {
        color_key = Object.keys(azerty_color_mode_set)[Math.floor(Math.random()*Object.keys(azerty_color_mode_set).length)];
      }
      paste_from(key, azerty_graphic_mode_set, 8*Math.floor(Math.random()*32), 8*Math.floor(Math.random()*32), 8*Math.floor(Math.random()*rdm), 8*Math.floor(Math.random()*rdm), azerty_color_mode_set[color_key][0][0], false, Scene);
    }
    clearImageData(Display);
    addImageDataTo(Scene,Display);
    ctx.scale(4,4);
    ctx.putImageData(Display, 0, 0);
    document.body.style.backgroundImage = "url("+canvas.toDataURL()+")";
    ctx.scale(1/4,1/4);
    clearImageData(Scene);
    draw();
    ctx.drawImage(intro_screen_img,0,0,width,height);
    let new_ImageData = ctx.getImageData(0,0,width,height);
    for (var i = 0; i < new_ImageData.data.length; i++) {
      Scene.data[i] = new_ImageData.data[i];
    }
    new_memory(Scene);
  } else {
    setTimeout(loading_listener);
  }
}
loading_listener();
