                    ____  _      _                         _ 
                   |  _ \(_)_  _| |__   ___   __ _ _ __ __| |
                   | |_) | \ \/ / '_ \ / _ \ / _` | '__/ _` |
                   |  __/| |)  (| |_) ( (_) ) (_| | | ( (_| |
                   |_|   |_/_/\_\_.__/ \___/ \__,_|_|  \__,_|
                                              _ __ _________ 
                                             / |  |_________:
                    ,..---~~~~~~~----....__.'  |  |_________:
                   (  ·=·   adl          __    |  |_________:
                    `~~---.......----~~~~  `.  |  |_________:
                                             \_|__|_________:

Pixboard is a simple keyboard-only raster graphics editor with a semi-graphic
input mode.

It allow to edit and produce 256x256 PNG images only.


Keybindings
-----------

| Key               | Function                |
|:----------------- |:----------------------- |
| F1                | change keyboard layout  |
| F2                | show intro screen       |
| F3                | show key-bindings       |
| F4                | show modes              |
| Insert            | fill with color         |
| Delete            | clear scene             |
| PageUp            | export PNG              |
| PageDown          | import PNG              |
| Home              | choose target color     |
| End               | deactivate target color |
| Tab               | change Mode             |
| Ctrl + z          | undo                    |
| Ctrl + y          | redo                    |
| Ctrl + c          | copy                    |
| Ctrl + v          | paste                   |
| Altl + a-Z 0-9 …  | change color            |
| Space             | erase                   |
| a-Z 0-9 …         | type pixels             |
| Ctrl + arrow keys | change cursor size      |
| arrow keys        | move cursor             |


This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you
are welcome to redistribute it under certain conditions (see GNU GPLv3 LICENSE).

[www.adelfaure.net](https://www.adelfaure.net)

Support me on [ko-fi.com/adelfaure](ko-fi.com/adelfaure)
